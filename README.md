# Heroku Buildpack: pdftotext

Loosely based on [Heroku NGIX buildpack][1].

Provides `pdftotext` command line tool from [Poppler pdf rendering library][2].

Contains updates from [eithed's fork][3], specifically the use of cmake in the build script.

## How to use if you're on Windows and have no clue what all this does

- Install Docker
- In Powershell, navigate to location with this README file in it, then run ```docker run -v ${PWD}:/buildpack --rm -it -e "STACK=heroku-XX" heroku/heroku:XX-build bash -c 'cd /buildpack; scripts/build_pdftotext /buildpack/bin/pdftotext-heroku-XX'```, where "XX" (3 instances!) is the Heroku stack number you want to build for, for instance "24".

Alternatively, for instance if you can not get the mount binding (-v flag) to work:

- Install Docker
- Find a way to install the heroku-XX image (```docker run "STACK=heroku-XX" heroku/heroku:XX-build``` should work - again, replace "XX" with the Heroku stack number you want to build for, for instance "24")
- Open the Docker GUI (Docker Desktop),
- Under Images, select the Heroku image and hit 'Run',
- Under 'Optional Settings', mount the heroku-buildpack-pdftotext folder as Host Path to Container Path "/buildpack" (*including* the / !)
- Run the Image, go to Containers, find the image with the right (Image) tags, click on the 3 vertical dots, and choose "Open in terminal" (icon looks like ```>_```).
- Inside the CLI window that pops up, run: ```cd /buildpack; scripts/build_pdftotext /buildpack/bin/pdftotext-heroku-XX``` (once again, replace "XX" with the Heroku stack number)

Both of these options should, after some downloading / compiling / copying, produce a new binary file in the 'bin' folder, named 'pdftotext-heroku-XX'.

You can delete the Docker containers and images afterwards, if any are left.

## Important notes and troubleshooting:

- Make sure the line endings of the files are set to ```LF```, otherwise they won't work in Docker.
- You can use the Docker Desktop app to navigate and manage containers / images, but you can **NOT** use it as an alternative to ```docker run...```, because the image will immediately exit.


[1]: https://github.com/heroku/heroku-buildpack-nginx
[2]: https://poppler.freedesktop.org/
[3]: https://github.com/eithed/heroku-buildpack-pdftotext